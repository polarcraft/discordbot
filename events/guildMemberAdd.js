const { EmbedBuilder } = require('discord.js');
const { logChannelId, infoChannelId } = require('../config.json');

module.exports = {
	name: 'guildMemberAdd',
	execute(member, client) {
		const channel = client.channels.cache.get(logChannelId);
		const infoChannel = client.channels.cache.get(infoChannelId);

		const sendEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setTitle(`${member.user.username} has joined!`)
			.setDescription(`Welkom ${member.user}! Lees de ${infoChannel} goed door voor de regels en uitleg!`)
			.setThumbnail(member.user.avatarURL());

		channel.send({ embeds: [sendEmbed] });
	},
};