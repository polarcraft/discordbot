const { EmbedBuilder } = require('discord.js');
const { logChannelId } = require('../config.json');

module.exports = {
	name: 'guildMemberRemove',
	execute(member, client) {
		const channel = client.channels.cache.get(logChannelId);

		const sendEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setTitle(`${member.user.username} has left!`);

		channel.send({ embeds: [sendEmbed] });
	},
};