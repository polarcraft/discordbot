const { ActivityType } = require('discord.js');

module.exports = {
	name: 'ready',
	once: true,
	execute(client) {
		console.log(`Ready! Logged in as ${client.user.tag}`);

		client.user.setActivity('Minecraft', { type: ActivityType.Playing });
		try {
			require('../functions/deploy-commands.js').init();
		} catch (e) {
			console.log(e);
		}
	},
};