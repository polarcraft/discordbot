const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits } = require('discord.js');
const sdk = require('node-appwrite');
const { logChannelId, appwriteKey } = require('../config.json');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('suspend')
		.setDescription('Suspend a user')
		.setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers)
		.addUserOption((option) => option
			.setName('user')
			.setDescription('The user you want to suspend')
			.setRequired(true))
		.addNumberOption((option) => option
			.setName('time')
			.setDescription('The time to suspend the user in minutes')
			.setRequired(true))
		.addStringOption((option) => option
			.setName('reason')
			.setDescription('The reason to suspend the user')
			.setRequired(true)),

	async execute(interaction, client) {
		const appwriteClient = new sdk.Client();
		const users = new sdk.Users(appwriteClient);
		const functions = new sdk.Functions(appwriteClient);

		appwriteClient
			.setEndpoint('https://appwrite.xeovalyte.com/v1')
			.setProject('62f0f5758fa678b3e1c9')
			.setKey(appwriteKey)
		;

		const user = await interaction.options.getUser('user');
		const time = await interaction.options.getNumber('time');
		const reason = await interaction.options.getString('reason');

		const logChannel = client.channels.cache.get(logChannelId);


		let filteredUser;
		try {
			const listUsers = await users.list();

			filteredUser = listUsers.users.find(item => item.prefs.discordId === user.id);

			if (!filteredUser) {
				const replyEmbed = new EmbedBuilder()
					.setColor('#74CBEB')
					.setDescription(`**${user} has no appwrite account. Use default discord method**`);

				return await interaction.reply({ embeds: [replyEmbed], ephemeral: true });
			}
		} catch (e) {
			console.log(e);
			const replyEmbed = new EmbedBuilder()
				.setColor('#74CBEB')
				.setDescription(`**${user} had no appwrite account. Use default discord method**`);

			return await interaction.reply({ embeds: [replyEmbed], ephemeral: true });
		}

		try {
			await functions.createExecution('suspend', JSON.stringify({ userId: filteredUser.$id, time: time, reason: reason }));
		} catch (e) {
			console.log(e);
			const replyEmbed = new EmbedBuilder()
				.setColor('#74CBEB')
				.setDescription('**Error while executing suspend function**');

			return await interaction.reply({ embeds: [replyEmbed], ephemeral: true });
		}

		const replyEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setDescription(`**Suspended ${user} for ${time} minutes, because ${reason}**`);

		const logEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setDescription(`**${interaction.user} suspended ${user} for ${time} minutes, because ${reason}**`);

		await interaction.reply({ embeds: [replyEmbed], ephemeral: true });
		await logChannel.send({ embeds: [logEmbed] });
	},
};