const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Replies with server ping'),
	async execute(interaction, client) {
		let replyEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.addFields(
				{ name: 'Websocket heartbeat', value: client.ws.ping + 'ms' },
				{ name: 'Roundtrip latency', value: 'Pinging...' },
			);

		const sent = await interaction.reply({ embeds: [replyEmbed], fetchReply: true });

		replyEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.addFields(
				{ name: 'Websocket heartbeat', value: client.ws.ping + 'ms' },
				{ name: 'Roundtrip latency', value: `${sent.createdTimestamp - interaction.createdTimestamp}ms` },
			);

		interaction.editReply({ embeds: [replyEmbed] });
	},
};