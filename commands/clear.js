const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('clear')
		.setDescription('Clears messages')
		.setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
		.addNumberOption(option => option
			.setName('amount')
			.setDescription('The amount of messages to be cleared (max 100)')
			.setRequired(true)),

	async execute(interaction) {
		const amount = interaction.options.getNumber('amount');

		let replyEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setTitle(`Cleared ${amount} message${amount > 1 ? 's' : ''}!`);

		if (amount > 100 || amount < 1) {
			if (amount > 100) replyEmbed = new EmbedBuilder().setColor('#74CBEB').setTitle('Cannot delete more then 100 messages at once');
			else if (amount < 1) replyEmbed = new EmbedBuilder().setColor('#74CBEB').setTitle('You must delete at least 1 message');

			return await interaction.reply({ embeds: [replyEmbed], ephemeral: true });
		}

		await interaction.channel.bulkDelete(amount).catch(e => new EmbedBuilder().setColor('#74CBEB').setTitle(`Could not delete messages because ${e}`));

		interaction.reply({ embeds: [replyEmbed], ephemeral: true });

	},
};