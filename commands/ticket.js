const { SlashCommandBuilder, EmbedBuilder, ChannelType, PermissionsBitField, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');
const { ticketCategoryId, moderatorRoleId, contributerRoleId } = require('../config.json');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ticket')
		.setDescription('Create a ticket')
		.addStringOption(option => option
			.setName('category')
			.setDescription('The category of the ticket')
			.setRequired(true)
			.addChoices(
				{ name: 'Griefing or Harm', value: 'griefing' },
				{ name: 'Discord Problem', value: 'discordProblem' },
				{ name: 'Minecraft Problem', value: 'minecraftProblem' },
				{ name: 'Website Problem', value: 'webProblem' },
				{ name: 'Other', value: 'other' },
			)),
	async execute(interaction) {
		const category = interaction.options.getString('category');

		let ticketChannel;
		if (category.includes('Problem')) {
			ticketChannel = await interaction.member.guild.channels.create({
				name: `ticket-${interaction.user.username}`,
				type: ChannelType.GuildText,
				parent: ticketCategoryId,
				permissionOverwrites: [
					{
						id: interaction.guild.id,
						deny: [PermissionsBitField.Flags.ViewChannel],
					},
					{
						id: moderatorRoleId,
						allow: [PermissionsBitField.Flags.ViewChannel],
					},
					{
						id: contributerRoleId,
						allow: [PermissionsBitField.Flags.ViewChannel],
					},
					{
						id: interaction.user.id,
						allow: [PermissionsBitField.Flags.ViewChannel],
					},
				],

			});
		} else {
			ticketChannel = await interaction.member.guild.channels.create({
				name: `ticket-${interaction.user.username}`,
				type: ChannelType.GuildText,
				parent: ticketCategoryId,
				permissionOverwrites: [
					{
						id: interaction.guild.id,
						deny: [PermissionsBitField.Flags.ViewChannel],
					},
					{
						id: moderatorRoleId,
						allow: [PermissionsBitField.Flags.ViewChannel],
					},
					{
						id: interaction.user.id,
						allow: [PermissionsBitField.Flags.ViewChannel],
					},
				],

			});
		}

		const newTicketEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setTitle('Ticket')
			.setDescription(`${interaction.user} created a ticket for **${category}**`);

		const row = new ActionRowBuilder()
			.addComponents(
				new ButtonBuilder()
					.setCustomId('close')
					.setLabel('Close Ticket')
					.setStyle(ButtonStyle.Danger),
			);

		const cancelRow = new ActionRowBuilder()
			.addComponents(
				new ButtonBuilder()
					.setCustomId('cancel')
					.setLabel('Cancel')
					.setStyle(ButtonStyle.Danger),
			);

		const cancelTicketEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setTitle('Closing ticket in 10 seconds...');

		const stopCancelTicketEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setTitle('Ticket closing has been stopped');


		const ticketMessage = await ticketChannel.send({ embeds: [newTicketEmbed], components: [row] });

		const collector = ticketMessage.createMessageComponentCollector();

		let ticketCancelMessage;
		let timeout;
		collector.on('collect', async i => {
			if (i.customId === 'close') {
				ticketCancelMessage = await ticketChannel.send({ embeds: [cancelTicketEmbed] });
				await ticketMessage.createMessageComponentCollector();
				await i.update({ components: [cancelRow] });
				timeout = setTimeout(() => ticketChannel.delete(), 10000);
			} else {
				clearTimeout(timeout);
				await ticketCancelMessage.edit({ embeds: [stopCancelTicketEmbed] });
				await i.update({ components: [row] });
			}
		});


		const replyEmbed = new EmbedBuilder()
			.setColor('#74CBEB')
			.setDescription(`${ticketChannel} has been created with the category **${category}**`);

		await interaction.reply({ embeds: [replyEmbed], ephemeral: true });


	},
};