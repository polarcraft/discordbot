const express = require('express');
const { guildId } = require('../config.json');

module.exports.init = (client) => {

	const app = express();
	app.use(express.json());

	app.listen(7289, () => console.log('API is online!'));

	app.get('/', (req, res) => {
		res.status(200).send({
			status: 'succes',
		});
	});

	app.post('/suspend', async (req, res) => {
		const { discordId, time, reason } = req.body;

		if (!discordId) return res.status(400).send({ code: 'no-discordid' });
		else if (!time) return res.status(400).send({ code: 'no-time' });

		const guild = await client.guilds.cache.get(guildId);
		const member = await guild.members.fetch(discordId);

		try {
			await member.timeout(time * 60 * 1000, reason);
			console.log(`${discordId} was timeout for ${reason}`);
		} catch (e) {
			console.log(e);
			return res.status(500).send({ code: 'error-timeout' });
		}

		res.status(200).send({ status: 'succes' });
	});

	app.post('/ban', async (req, res) => {
		const { discordId, reason } = req.body;

		if (!discordId) return res.status(400).send({ code: 'no-discordid' });

		const guild = await client.guilds.cache.get(guildId);

		try {
			await guild.members.ban(discordId, { reason: reason });
			console.log(`${discordId} was banned for ${reason}`);
		} catch (e) {
			console.log(e);
			return res.status(500).send({ code: 'error-ban' });
		}

		res.status(200).send({ status: 'succes' });
	});
};
